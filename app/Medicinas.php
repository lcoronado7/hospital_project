<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicinas extends Model
{
   	protected $fillable = [
        'nombre','nombre_cientifico','gramos','tipoEstado',
    ];
    
    public function HistorialMedicina(){
    	return $this->hasMany('App\HistorialMedicina');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    
}
