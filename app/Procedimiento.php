<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedimiento extends Model
{
    protected $fillable = [
        'nombre','tipo_Procedimiento','nivel_de_riesgo','costo','requisitos_estado_salud','ambulatorio','descripcion',
    ];
    
    
    public function HistorialProcedimiento(){
    	return $this->hasMany('App\HistorialProcedimiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
