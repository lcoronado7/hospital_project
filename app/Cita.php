<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $fillable = [
        'fecha_cita','tipo_solicitud','fecha_consulta','hora_consulta','estado_cita','descripcion','id_paciente','id_medico',
    ];
    public function Medico(){
    	return $this->belongsTo('App\Medico');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Paciente(){
    	return $this->belongsTo('App\Paciente');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Diagnostico(){
    	return $this->belongsTo('App\Diagnostico');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
