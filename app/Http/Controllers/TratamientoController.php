<?php

namespace App\Http\Controllers;

use App\Tratamiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class TratamientoController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'descripcion_Medicamentos' => ['required', 'string'],
            'descripcion_Operaciones' => ['required', 'string'],
            'id_diagnostico' => ['required', 'string'],
      
            
        ]);
    }
    public function registro(){

    	return view('auth.registertratamiento'); 
    }
    protected function create(array $data)
    {
        return Tratamiento::create([
            'descripcion_Medicamentos' => $data['descripcion_Medicamentos'],
            'descripcion_Operaciones' => $data['descripcion_Operaciones'],
            'id_diagnostico' => $data['id_diagnostico'],
        ]);
    }
}
