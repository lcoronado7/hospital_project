<?php

namespace App\Http\Controllers;

use App\Cita;
use App\Persona;
use App\Paciente;
use App\Medico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class CitaController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'fecha_cita' => ['required', 'string'],
            'tipo_solicitud' => ['required', 'string'],
            'fecha_consulta' => ['required', 'string'],
            'hora_consulta' => ['required', 'string'],
            'estado_cita' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'cedulaPaciente' => ['required', 'string'],
            'cedulaMedico' => ['required', 'string'],
            
        ]);
    }
    public function registro(){

    	return view('auth.registercitas'); 
    }
    public function show(){
        $citas=Cita::all();
        if(!is_null($citas)){
           

            return view('tablecitas')->with(compact('citas')); 
        }
        

        return view('tablecitas'); 
    }
    public function showCitaMedico(Request $request){
        $persona=Persona::where('cedula',$request->input('cedula'))->first();
        $medico=Medico::where('id_persona',$persona['id'])->first();
        $citas=DB::table('citas')->where('id_medico',$medico['id'])->select("fecha_cita","tipo_solicitud","fecha_consulta","hora_consulta","estado_cita","id_paciente")->get();
            return view('tablecitasMedico')->with(compact('citas')); 
        
        }
    protected function create(array $data)
    {
        $pasCedula=DB::table('personas')->where('cedula',$data['cedulaPaciente'])->value('id');
        $mediCedula=DB::table('personas')->where('cedula',$data['cedulaMedico'])->value('id');
        $paciente=DB::table('pacientes')->where('id_persona',$pasCedula)->value('id');
        $medico=DB::table('medicos')->where('id_persona',$mediCedula)->value('id');
        
        return Cita::create([
            'fecha_cita' => $data['fecha_cita'],
            'tipo_solicitud' => $data['tipo_solicitud'],
            'fecha_consulta' => $data['fecha_consulta'],
            'hora_consulta' => $data['hora_consulta'],
            'estado_cita' => $data['estado_cita'],
            'descripcion' => $data['descripcion'],
            'id_paciente' => $paciente,
            'id_medico' => $medico,

        ]);
    }
}
