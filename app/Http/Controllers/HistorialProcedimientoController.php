<?php

namespace App\Http\Controllers;

use App\HistorialProcedimiento;
use App\Procedimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class HistorialProcedimientoController extends Controller
{
	 use RegistersUsers;

	protected function validator(array $data)
    {

        return Validator::make($data, [
            'fecha_realizacion' => ['required', 'string'],
            'estado_realizacion' => ['required', 'string'],
            'id_procedimiento' => ['required', 'int'],
            'id_tratamiento' => ['required', 'string'],
            
        ]);
    }
    public function show(){
        $hp=HistorialProcedimiento::all();
        if(!is_null($hp)){
           

            return view('tablehistorialprocedimientos')->with(compact('hp')); 
        }
        

        return view('tablehistorialprocedimientos'); 
    }
    public function registro(){
        $procedimientos=Procedimiento::all();
    	return view('auth.registerhistorialProcedimiento')->with(compact('procedimientos')); 
    }
    protected function create(array $data)
    {
        return HistorialProcedimiento::create([
            'fecha_realizacion' => $data['fecha_realizacion'],
            'estado_realizacion' => $data['estado_realizacion'],
            'id_tratamiento' => $data['id_tratamiento'],
            'id_procedimiento' => $data['id_procedimiento'],

        ]);
    }



}
