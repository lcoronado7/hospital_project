<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Paciente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;


class PacienteController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'peso' => ['required', 'string'],
            'altura' => ['required', 'string'],
            'cedula'=> ['required', 'string'],
            
        ]);
    }
    public function registro(){

    	return view('auth.registerpaciente'); 
    }
    public function showlogin(){

        return view('auth.loginpaciente'); 
    }
    public function openHome(Request $request){
        //$persona=DB::table('personas')->where('cedula',$request->input('cedula'))->value("cedula");
        $persona=Persona::where('cedula',$request->input('cedula'))->first();
        $passwordEn= $request->input('password');
        $cedula=$persona['cedula'];
        if(($request->input('cedula')==$persona['cedula']) && password_verify($passwordEn,$persona['password'])){
            return view('homepaciente')->with(compact('cedula'));
        }
        
        return back()->with('msj',"Error de credenciales"); 
    }
    public function show(){
        $Paciente=Paciente::all();
        if(!is_null($Paciente)){
            $personasPas=array($Paciente->count());
            $pacientedato=array($Paciente->count());
            $countpaciente=0;
            foreach ($Paciente as $key => $pas) {
                $personasPas[$countpaciente]=DB::table('personas')->where('id',$pas['id_persona'])->select("name","apellido","cedula","telefono_Personal","email","direccion")->get();
                $pacientedato[$countpaciente]=$pas;
                $countpaciente+=1;
            }
            $cantpaciente=count($personasPas);
            return view('tablepersona')->with(compact('personasPas','pacientedato')); 
        }
        

        return view('tablepersona'); 
    }
    public function preedit($cedula){
         $persona=Persona::where('cedula',$cedula)->first();
         return view('auth.editpaciente')->with(compact('persona'));
    }
    public function edit(Request $request,$cedula){
        $nombreNuevo=$request->input('name');

        $persona=Persona::find(1);

        $persona->name=$nombreNuevo;
        $persona->save();
        return $this->show();
    }

    protected function create(array $data)
    {
        $persona=DB::table('personas')->where('cedula',$data['cedula'])->value('id');
        return Paciente::create([
            'peso' => $data['peso'],
            'altura' => $data['altura'],
            'id_persona' =>  $persona,
        ]);
    }
}
