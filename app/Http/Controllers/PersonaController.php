<?php

namespace App\Http\Controllers;


use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class PersonaController extends Controller
{
	use RegistersUsers;

    protected function credentials(Request $request)
    {
         $login = $request->input($this->username());

        // Comprobar si el input coincide con el formato de E-mail
         $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? ‘cedula’ : ‘email’;

        return [
         $field => $login,
         ‘password’ => $request->input(‘password’)
         ];
    }
	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string'],
            'apellido' => ['required', 'string'],
            'cedula' => ['required', 'string', 'max:255', 'unique:personas'],
            'fecha_nacimiento' => ['required', 'string', 'max:255'],
            'edad' =>['required', 'int', 'max:255'],
            'sexo' =>['required', 'string', 'max:255'],
            'tipo_Sangre' =>['required', 'string', 'max:255'],
            'telefono_Personal' => ['required', 'string', 'max:255'],
            'telefono_Emergencia' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:personas'],
            'direccion' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    public function registro(){

    	return view('auth.registerpersona'); 
    }

     protected function create(array $data)
    {
        return Persona::create([
            'name' => $data['name'],
            'apellido' => $data['apellido'],
            'cedula' => $data['cedula'],
            'fecha_nacimiento' => $data['fecha_nacimiento'],
            'edad' => $data['edad'],
            'sexo' => $data['sexo'],
            'tipo_Sangre' => $data['tipo_Sangre'],
            'telefono_Personal' => $data['telefono_Personal'],
            'telefono_Emergencia' => $data['telefono_Emergencia'],
            'email' => $data['email'],
            'direccion' => $data['direccion'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
