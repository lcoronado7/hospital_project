<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialEspecialidad extends Model
{
    protected $fillable = [
        'nivel','casa_Estudio','fecha_Inicio_Ejercion','id_medico','id_especialidad',
    ];
    public function Medico(){
    	return $this->belongsTo('App\Medico');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Especialidad(){
    	return $this->belongsTo('App\Especialidad');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
