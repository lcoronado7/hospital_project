<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
	protected $fillable = [
        'fecha_IngresoHospital','estado_Contrato', 'id_persona',
    ];
    public function persona(){
    	return $this->belongsTo('App\Persona');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function cita(){
        return $this->hasMany('App\Cita');
        //hasMany(Persona::class) para relaciones de 1 a M
    }
    public function historialespecialidad(){
    	return $this->hasMany('App\HistorialEspecialidad');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
