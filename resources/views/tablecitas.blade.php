@extends('layouts.app')
@section('content')
<div class="container" style="justify-content: center;">
<div class="limiter">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b-110">
                <table data-vertable="ver1">
                    <thead>
                        <tr class="row100 head">
                            <th class="column100 column1" data-column="column1">Fecha Cita</th>
                            <th class="column100 column2" data-column="column2">Tipo de Solicitud</th>
                            <th class="column100 column3" data-column="column3">Fecha Consulta</th>
                            <th class="column100 column4" data-column="column4">Hora Consulta</th>
                            <th class="column100 column5" data-column="column5">Esatdo Cita</th>
                            <th class="column100 column6" data-column="column6">ID paciente</th>
                            <th class="column100 column7" data-column="column7">ID medico</th>
                            <th class="column100 column8" data-column="column8">ID cita</th>
                            <th class="column100 column9" data-column="column9">Editar</th>
                            <th class="column100 column10" data-column="column10">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($citas as $k => $cita)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-column="column1">{{$cita->fecha_cita}}</td>
                                <td class="column100 column2" data-column="column2">{{$cita->tipo_solicitud}}</td>
                                <td class="column100 column3" data-column="column3">{{$cita->fecha_consulta}}</td>
                                <td class="column100 column4" data-column="column4">{{$cita->hora_consulta}}</td>
                                <td class="column100 column5" data-column="column5">{{$cita->estado_cita}}</td>
                                <td class="column100 column6" data-column="column6">{{$cita->id_paciente}}</td>
                                <td class="column100 column7" data-column="column7">{{$cita->id_medico}}</td>
                                <td class="column100 column8" data-column="column8">{{$cita->id}}</td>
                                <td class="column100 column9" data-column="column9"><a href="/#"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column10" data-column="column10"><a href="/#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>
                        @endforeach


                    
                    </tbody>
                </table>
            </div>
        </div>
     
    </div>

</div>
@endsection