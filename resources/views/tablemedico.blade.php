@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b110">
                <table data-vertable="ver1">
                    <thead>
                        <tr class="row100 head">
                            <th class="column100 column1" data-column="column1">Nombre</th>
                            <th class="column100 column2" data-column="column2">Apellido</th>
                            <th class="column100 column3" data-column="column3">Cedula</th>
                            <th class="column100 column4" data-column="column4">Telefono Personal</th>
                            <th class="column100 column5" data-column="column5">Email</th>
                            <th class="column100 column6" data-column="column6">Direccion</th>
                            <th class="column100 column7" data-column="column7">Fecha Ingreso Hospital</th>
                            <th class="column100 column8" data-column="column8">Estado Contrato</th>
                            <th class="column100 column9" data-column="column9">Editar</th>
                            <th class="column100 column10" data-column="column10">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($personasMed as $k => $med)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-column="column1">{{$med[0]->name}}</td>
                                <td class="column100 column2" data-column="column2">{{$med[0]->apellido}}</td>
                                <td class="column100 column3" data-column="column3">{{$med[0]->cedula}}</td>
                                <td class="column100 column4" data-column="column4">{{$med[0]->telefono_Personal}}</td>
                                <td class="column100 column5" data-column="column5">{{$med[0]->email}}</td>
                                <td class="column100 column6" data-column="column6">{{$med[0]->direccion}}</td>
                                <td class="column100 column7" data-column="column7">{{$Medicodato[0]->fecha_IngresoHospital}}</td>
                                <td class="column100 column8" data-column="column8">{{$Medicodato[0]->estado_Contrato}}</td>
                                <td class="column100 column9" data-column="column9"><a href="/#"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column10" data-column="column10"><a href="/#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>
                        @endforeach


                    
                    </tbody>
                </table>
            </div>
        </div>

</div>
@endsection