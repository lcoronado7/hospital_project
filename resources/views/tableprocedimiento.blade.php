@extends('layouts.app')
@section('content')
<div class="container" style="justify-content: center;">
<div class="limiter">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b-110">
                <table data-vertable="ver1">
                    <thead>
                        <tr class="row100 head">
                            <th class="column100 column1" data-column="column1">Nombre</th>
                            <th class="column100 column2" data-column="column2">Tipo de Procedimiento</th>
                            <th class="column100 column3" data-column="column3">Nivel de Riesgo</th>
                            <th class="column100 column4" data-column="column4">Costo</th>
                            <th class="column100 column5" data-column="column5">Requisitos</th>
                            <th class="column100 column6" data-column="column6">Ambulatorio</th>
                            <th class="column100 column7" data-column="column7">Descripcion</th>
                            <th class="column100 column8" data-column="column9">Editar</th>
                            <th class="column100 column9" data-column="column10">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($procedimiento as $k => $pro)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-column="column1">{{$pro->nombre}}</td>
                                <td class="column100 column2" data-column="column2">{{$pro->tipo_Procedimiento}}</td>
                                <td class="column100 column3" data-column="column3">{{$pro->nivel_de_riesgo}}</td>
                                <td class="column100 column4" data-column="column4">{{$pro->costo}}</td>
                                <td class="column100 column5" data-column="column5">{{$pro->requisitos_estado_salud}}</td>
                                <td class="column100 column6" data-column="column6">{{$pro->ambulatorio}}</td>
                                <td class="column100 column7" data-column="column7">{{$pro->descripcion}}</td>
                                <td class="column100 column8" data-column="column8"><a href="/#"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column9" data-column="column9"><a href="/#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>
                        @endforeach


                    
                    </tbody>
                </table>
            </div>
        </div>
     
    </div>

</div>
@endsection