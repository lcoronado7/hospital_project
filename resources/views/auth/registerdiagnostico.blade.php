@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerdiagnostico') }}">
                        @csrf
                         <div class="form-group row">
                            <label for="nombre_clave" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Clave') }}</label>

                            <div class="col-md-6">
                                <input id="nombre_clave" type="text" class="form-control{{ $errors->has('nombre_clave') ? ' is-invalid' : '' }}" name="nombre_clave" value="{{ old('nombre_clave') }}" required autofocus>

                                @if ($errors->has('nombre_clave'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre_clave') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion" type="text" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} z-depth-1" name="descripcion" value="{{ old('descripcion') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="id_cita" class="col-md-4 col-form-label text-md-right">{{ __('Identificador Cita') }}</label>

                            <div class="col-md-6">
                                <input id="id_cita" type="text" class="form-control{{ $errors->has('id_cita') ? ' is-invalid' : '' }}" name="id_cita" value="{{ old('id_cita') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('id_cita'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_cita') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection