@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerMedicina') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Medicina') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nombre_cientifico" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Cientifico Medicina') }}</label>

                            <div class="col-md-6">
                                <input id="nombre_cientifico" type="text" class="form-control{{ $errors->has('nombre_cientifico') ? ' is-invalid' : '' }}" name="nombre_cientifico" value="{{ old('nombre_cientifico') }}" required autofocus>

                                @if ($errors->has('nombre_cientifico'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre_cientifico') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gramos" class="col-md-4 col-form-label text-md-right">{{ __(' Gramos') }}</label>

                            <div class="col-md-6">
                                <input id="gramos" type="text" class="form-control{{ $errors->has('gramos') ? ' is-invalid' : '' }}" name="gramos" value="{{ old('gramos') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('gramos'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('gramos') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Tipo Estado Medicamento</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="tipoEstado" id="tipoEstado">
                                  <option selected>Escoge el Tipo Estado Medicamento</option>
                                  <option value="Jarabe">Jarabe</option>
                                  <option value="Pastilla">Pastilla</option>
                                  <option value="Intravenoso">Intravenoso</option>
                                </select>
                            </div>                            
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection