@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registercitas') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Cita</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_cita" type="text" readonly  name="fecha_cita" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Tipo Solicitud</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="tipo_solicitud" id="tipo_solicitud">
                                  <option selected>Escoge el Tipo de Solicitud</option>
                                  <option value="Telefonica">Telefonica</option>
                                  <option value="Personal">Personal</option>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Consulta</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_consulta" type="text" readonly  name="fecha_consulta" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Hora Consulta</label>
                            <div class="col-md-6"> 
                                <div class="controls">
                                    <div class="clockpicker-with-callbacks">
                                        <input type="text" class="form-control" value="10:10" id="hora_consulta" name="hora_consulta" readonly>
                                    </div>
                                </div>       
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Estado Cita</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="estado_cita" id="estado_cita">
                                  <option selected>Escoge tu Estado de Cita</option>
                                  <option value="Espera">Espera</option>
                                  <option value="Cancelada">Cancelada</option>
                                  <option value="Realizada">Realizada</option>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion" type="text" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} z-depth-1" name="descripcion" value="{{ old('descripcion') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cedulaPaciente" class="col-md-4 col-form-label text-md-right">{{ __('Cedula de Identidad del Paciente') }}</label>

                            <div class="col-md-6">
                                <input id="cedulaPaciente" type="text" class="form-control{{ $errors->has('cedulaPaciente') ? ' is-invalid' : '' }}" name="cedulaPaciente" value="{{ old('cedulaPaciente') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('cedulaPaciente'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cedulaPaciente') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cedulaMedico" class="col-md-4 col-form-label text-md-right">{{ __('Cedula de Identidad del Medico') }}</label>

                            <div class="col-md-6">
                                <input id="cedulaMedico" type="text" class="form-control{{ $errors->has('cedulaMedico') ? ' is-invalid' : '' }}" name="cedulaMedico" value="{{ old('cedulaMedico') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('cedulaMedico'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cedulaMedico') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection