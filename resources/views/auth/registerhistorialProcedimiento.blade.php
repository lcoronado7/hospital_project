@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerhistorialProcedimiento') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Realizacion Procedimiento</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_realizacion" type="text" readonly  name="fecha_realizacion" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Estado Realizacion Procedimiento</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="estado_realizacion" id="estado_realizacion">
                                  <option selected>Estado del Procedimiento</option>
                                  <option value="Espera">Espera</option>
                                  <option value="Cancelada">Cancelada</option>
                                  <option value="Realizada">Realizada</option>
                                </select>
                            </div>                            
                        </div>
                        
                      
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Procedimiento a Realizar</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="id_procedimiento" id="id_procedimiento">
                                  <option selected>Escoge tu Estado Contrato</option>
                                  @foreach ($procedimientos as $process)
                                    <option value={{$process->id}}>{{$process->nombre}}</option>
                                  @endforeach
                                </select>
                            </div>                            
                        </div>
                        
                        
                        <div class="form-group row">
                            <label for="id_tratamiento" class="col-md-4 col-form-label text-md-right">{{ __('Identificador Tratamiento') }}</label>

                            <div class="col-md-6">
                                <input id="id_tratamiento" type="text" class="form-control{{ $errors->has('id_tratamiento') ? ' is-invalid' : '' }}" name="id_tratamiento" value="{{ old('id_tratamiento') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('id_tratamiento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_tratamiento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection