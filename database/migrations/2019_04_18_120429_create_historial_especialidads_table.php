<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialEspecialidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_especialidads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nivel');
            $table->string('casa_Estudio');
            $table->date('fecha_Inicio_Ejercion');
            $table->bigInteger('id_medico')->unsigned();
            $table->foreign('id_medico')->references('id')->on('medicos');
            $table->bigInteger('id_especialidad')->unsigned();
            $table->foreign('id_especialidad')->references('id')->on('especialidads');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_especialidads');
    }
}
